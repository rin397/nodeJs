import { Component, OnInit } from '@angular/core';

import {Router} from "@angular/router";
import {DataService} from "../data.service";
import {RestApiService} from "../rest-api.service";
import {validate} from "codelyzer/walkerFactory/walkerFn";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email="";
  password="";
  btnDisbled = false;

  constructor(private router:Router , private data:DataService , private rest:RestApiService) { }

  ngOnInit() {
  }

  validate(){
    if(this.email){
      if(this.password){
        return true;
      }else{
        this.data.error("please check password id done!");
      }
    }else {
      this.data.error("pleaser check email is done!!");
    }
  }

  async login(){
    this.btnDisbled = true;

    try {

      if(this.validate()){
        const data = this.rest.post(
          'http://localhost:3030/api/accounts/login',
          {
            email :this.email,
            password: this.password
          });
        if(data['success']){
          localStorage.setItem("token",data['token']);
          this.router.navigate(['/']);
        }else{
          this.data.error(data['message']);
        }
      }
    }catch (error) {
      this.data.error(error['message']);
    }
    this.btnDisbled = false;

  }

}
