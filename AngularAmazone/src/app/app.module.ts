import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

/* import app component*/
import { AppComponentItem } from './app.component';

import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './home/home.component'

import { RestApiService } from "./rest-api.service";
import { MessageComponent } from './message/message.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  /* the application's lone component, which is also */
  declarations: [
    AppComponentItem,
    HomeComponent,
    MessageComponent,
    RegisterComponent,
    LoginComponent
  ],
  /* that this and every application needs to run in a browser. */
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    HttpClientModule,
    FormsModule
  ],
  providers: [
    RestApiService
  ],
  /* the root component that Angular creates and inserts into the index.html host web page */
  /* The AppComponent selector — here and in most documentation samples — is my-app so Angular looks for a <my-app> tag in the index.html like this one ... */
  bootstrap: [AppComponentItem]
})
export class AppModule { }
