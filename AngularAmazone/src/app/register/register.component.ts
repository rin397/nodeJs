import { Component, OnInit } from '@angular/core';

import {RestApiService} from "../rest-api.service";
import {DataService} from "../data.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  name = '';
  email ='';
  password ='';
  password1 ='';
  isSeller = false;
  btnDisabled = false;

  " ultiple constructor implementations are not allowed"
 " constructor() { }"
  constructor(private router:Router , private data:DataService,private rest:RestApiService){}

  ngOnInit() {
  }

  validate(){
    if (this.name){
      if(this.email){
        if(this.password){
          if(this.password1){
            if(this.password != this.password1){
            return true;
            }else {
              this.data.error('check the password is the same!!');
            }
          }else {
            this.data.error('please enter password1!!');
          }
        }else {
          this.data.error('please enter password!!');
        }
      }else {
        this.data.error('please enter email!!');
      }
    } else {
      this.data.error('please enter name!!');
    }
  }

  async register(){
    this.btnDisabled = true;
    try {
      if(this.validate()){
        const data = await this.rest.post(
          'http://localhost:3030/api/accounts/signup',
          {
            name: this.name,
            email: this.email,
            password: this.password,
            isSeller: this.isSeller
          }
        );
        if(data['success']){
          localStorage.setItem('token',data['token']);
          this.data.success('register success!!');
        }else {
          this.data.error(data['error']);
        }
      }
    }catch (err) {
      this.data.error(err['message']);
    }
    this.btnDisabled = false;
  }

}
