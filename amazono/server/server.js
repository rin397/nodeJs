const express = require('express');
const morgan = require('morgan');
const bodyParse = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

const config = require('./config');

const app = express();

mongoose.connect(config.database,(err) =>{
	if(err){
		console.log(err);
	}else{
		console.log('connect')
	}
})

app.use(bodyParse.json());
app.use(bodyParse.urlencoded({extened: false}));
app.use(morgan('dev'));
app.use(cors())

app.listen(config.port,err =>{
	console.log('magic happens on port ',config.database);
});

// app.get('/',(req,res,next) => {
// 	res.json({
// 		user: 'Wilson!'
// 	});
// });

const account = require('./routes/account');
app.use('/api/accounts/',account);