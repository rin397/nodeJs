const router = require('express').Router();
const jwt = require('jsonwebtoken');

const User = require('../models/user');

const config = require('../config');


router.post('/signup',(req,res,next) => {

    console.log(req.body);
    let user = new User();

    user.email = req.body.email;
    user.password = req.body.password;
    user.picture - user.gravatar();
    user.isSeller = req.body.isSeller;

    User.findOne({email: req.body.email},(err,existUser) =>{
        if(existUser){
            res.json({
               susccess: false,
               message: 'Account with email is already exist'
            });
        }else{
            user.save();
            var token = jwt.sign({
                user:user
            },config.secret,{expiresIn: '7d'});

            res.json({
                susccess: true,
                message: 'Enjoy',
                token: token
            });
        }
    });
});

router.post('/signin',(req,res,next)=>{
    console.log(req.body);
    User.findOne({
        email: req.body.email
    },(err,user)=>{
        if(!user){
            res.json({
                susccess: false,
                message: 'Account not exist'
            });
        }else{
            var validatorPassword = user.comparePassword(req.body.password);
            if(!validatorPassword){
                res.json({
                    susccess: false,
                    message: 'Password is not correct!'
                });
            }else{

                var token = jwt.sign({
                    user:user
                },config.secret,{expiresIn: '7d'});


                res.json({
                    susccess: false,
                    message: 'Login OK',
                    token: token
                });
            }
        }
    })
});

module.exports = router;