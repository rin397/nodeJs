const mongoose = require('mongoose');
const schema = mongoose.Schema;


const bcrypt = require('bcrypt-nodejs');

/* node利用 OpenSSL库来实现它的加密技术 */
const crypto = require('crypto');

const UserSchema = new schema({
    email:{type:String , unique:true , lowercase: true},
    name :String,
    password:String,
    picture:String,
    isSeller:{type:Boolean,default:false},
    address:{
        addr1:String,
        addr2:String,
        city:String,
        state:String,
        country:String,
        postalCode:String
    },
    createDate:{type:Date,default:Date.now()}
});



/* pre save function next next(err)
 *
 *  function process next(->) save
 *  function process next(err) exception
 *
 *  call back function vs promise
 *  */
UserSchema.pre('save',function (next) {
    var user = this;
    if(!user.isModified('password')) return next();


    /*  Store hash in your password DB */
    bcrypt.hash(user.password, null , null , function(err,hash){
        if(err) return next(err);
        user.password = hash;
        next();

    })
});

UserSchema.methods.comparePassword = function(password){
    return bcrypt.compareSync(password, this.password);
};

UserSchema.methods.gravatar = function(size){

    if(!this.size) size = 200;

    if(!this.email){

        return 'https://gravatar.com/avatar/?s'+size+'&=reto';
    } else {

        /*
        1. 生成了一个md5的hash实例
        2. hash.update()方法 ,有记忆功能 ,  hash.update()方法就是将字符串相加
        3. md5.digest(‘hex’); 期望以16进制的形式打印md5值
         *  */
        var md5 = crypto.createHash('md5').update(this.email).digest('hax');

        return 'https://gravatar.com/avatar/'+md5+'/?s'+size+'&=reto';
    }

};

module.exports = mongoose.model('User',UserSchema);